<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process. -->

> **This issue template is DEPRECATED. Please follow the new process in https://handbook.gitlab.com/handbook/marketing/blog/#how-to-suggest-a-blog-idea---new-process**
